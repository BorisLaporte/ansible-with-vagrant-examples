# Ansible with Vagrant examples

This project is a collection of examples on how to use Ansible with Vagrant.

# Setup

```bash
# Create a python virtual env
python3 -m venv .venv --prompt=ansible-vagrant-examples

# Install requirements (only Ansible is installed)
pip install -r requirements.txt
```

# Examples

## Example Voting App

Install Docker on one Debian 9 machine, download the [example-voting-app](https://github.com/dockersamples/example-voting-app) and deploys it.


```bash
# /example-voting-app

# Create the virtual machine and run the ansible script
vagrant up

# If you wish to re run the ansible script or if it has failed
vagrant provision

# Connect to the virtual machine using ssh
# the private IP address is defined in Vagrantfile
ssh -i .vagrant/machines/default/virtualbox/private_key vagrant@192.168.77.21
```

Once completed, to check if everything is working as desired open your browser to `http://192.168.77.21:5000/` or `http://192.168.77.21:5001/`